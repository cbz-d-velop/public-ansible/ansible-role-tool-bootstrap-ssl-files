#!/bin/bash
set -e

# Variables to track global status
global_status="SUCCESS"
type="post-commit"
DIR=$(basename "$(pwd)")

# Function to run a command and report its status
run_and_check() {
    local description="$1"
    local cmd="$2"
    
    echo -e "Action: from script ${type}: ${description}: IN PROGRESS"
    if eval "$cmd"; then
        status="SUCCESS"
    else
        status="FAILED"
        global_status="FAILED"
    fi
    echo -e "Action: from script ${type}: ${description}: ${status}"
}

# Define two arrays: one for descriptions and one for commands
descriptions=(
    "Decrypt the local vault file"
)

commands=(
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"$HOME/.docker:/root/.docker\" -v \"\$(pwd):/root/ansible/\${DIR}\" -w /root/ansible/\${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && (ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml || true)'"
)

# Run all commands in the specified order
echo "====================== Git hook: ${type} ======================"
for i in "${!descriptions[@]}"; do
    run_and_check "${descriptions[i]}" "${commands[i]}"
    echo "--------------------------------------------------------"
done

# Report the global status
if [ "$global_status" = "SUCCESS" ]; then
    result=0
else
    result=1
fi

echo "====================== Git hook: ${type} ======================"
echo -e "Report: all actions done for hook ${type}, global status: ${global_status}"
echo "====================== Git hook: ${type} ======================"
exit $result
