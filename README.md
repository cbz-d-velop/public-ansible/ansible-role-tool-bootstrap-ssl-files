# Ansible role: tool.bootstrap_ssl_files

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20obin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: OpenSSL](https://img.shields.io/badge/Tech-OpenSSL-orange)
![Tag: SSL/TLS](https://img.shields.io/badge/Tech-SSL/TLS-orange)

An Ansible role to create a list of defined files related to SSL/TLS for your Ansible project.

The Certificate Management role automates the creation and organization of certificates, including Certificate Authorities (CAs), root CAs, intermediate CAs, and end certificates. This role simplifies the process of generating certificates and provides a streamlined approach for managing SSL/TLS infrastructure. Key features of this role include:

Certificate Creation: The role generates various types of certificates, including root CAs, intermediate CAs, and end certificates. It allows you to define the desired SSL/TLS information, such as Common Name (CN), Country (C), State (ST), Locality (L), Organization (O), Organizational Unit (OU), and email address.

Certificate Filing: The role organizes the generated certificates into separate components and creates a ZIP archive for each component. This makes it easy to deploy and distribute the certificates to the desired target systems.

Customization Options: The role provides flexibility by allowing you to specify the validity period, key size, and base path for storing the certificate files. You can customize these parameters to meet your specific requirements.

By utilizing the Certificate Management role, you can simplify the creation and management of SSL/TLS certificates, ensure proper organization and filing of certificates, and streamline the deployment process.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-05-04: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez (see [here](https://youtu.be/T_BbKnzYfP0))

### 2023-04-20: P12 Files

* Now the role can create P12 files, password protected
* Removed the boolean indicator in order to create a full bundle of file
* Probably need a CA ? Maybe it will be added ...

### 2023-05-27: CA Authorities and bundle

* Now we can create a CA Authority to sign a cert
* We can do that with "bootstrap_ssl_files__ca_name", "bootstrap_ssl_files__ca" and "bootstrap_ssl_files__cert" vars
* "bootstrap_ssl_files__ca" + "bootstrap_ssl_files__ca_name" => Create CA
* "bootstrap_ssl_files__name" + "bootstrap_ssl_files__cert" => Create CERT and sign with the CA from "bootstrap_ssl_files__ca_file"
* "bootstrap_ssl_files__cert" just create CERT
* Role make zip bundles (all files for a CA and for CERT) in a defined location by "bootstrap_ssl_files__dest"

### 2023-05-30: JKS file

* jks file added
* jks is convert in pkcs12 format

### 2023-05-30: PKC8 Key file

* file.kcs8.key file added, because some Java apps doesn't work with pkcs12

### 2023-06-28: Open source and refactoring

* Role dont create P12/P8/JKS files, because its a better practise to create desired files directly in the deployment role

### 2023-07-23: CA chains reworked, BREAKING CHANGES

* You can now define a totally custom PKI plateform
* You can create multile intermediaite CA with the level that you want (999 deep lenght max)
* Certificates can have any level of CA validation (ROOT, CA 1, CA 2 etc)
* CAs keys are password protected
* To deploy a cert, you have the ca-chain.pem.crt, that contains all cert of the structure for that cert
* Revocation system not implemented

### 2023-07-23: Revoke cert

* Role handle the revocation of certs if exist

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2024-02-29: New CI

* Added new CI
* Added SonarQube
* Refact vars

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-07: Ansible Vault and automations

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation

### 2024-12-31: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
